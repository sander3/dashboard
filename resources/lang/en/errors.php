<?php

return [

    'already_sent' => 'Notification can not be changed because it has already been sent.',

];
