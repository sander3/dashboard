<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['summary', 'description'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['user_id'];

    /**
     * Get the user that has sent the notification.
     */
    public function sender()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Get the contact that has received the notification.
     */
    public function receiver()
    {
        return $this->hasOne('App\Contact', 'contact_id');
    }
}
