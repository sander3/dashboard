<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    /**
     * Get the users associated with the contact.
     */
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    /**
     * Get the notifications associated with the contact.
     */
    public function notifications()
    {
        return $this->belongsTo('App\Notification');
    }
}
