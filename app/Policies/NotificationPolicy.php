<?php

namespace App\Policies;

use App\Notification;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class NotificationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given notification can be shown to the user.
     *
     * @param  \App\User $user
     * @param  \App\Notification $notification
     * @return bool
     */
    public function show(User $user, Notification $notification)
    {
        return $user->id == $notification->user_id;
    }

    /**
     * Determine if the given notification can be updated by the user.
     *
     * @param  \App\User $user
     * @param  \App\Notification $notification
     * @return bool
     */
    public function update(User $user, Notification $notification)
    {
        return $user->id == $notification->user_id;
    }
}
