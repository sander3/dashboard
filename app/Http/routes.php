<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

// API
Route::group([
    'middleware' => [
        'api',
        'auth.basic.once',
    ],
    'namespace'  => 'Api',
    'prefix'     => 'api',
], function () {
    // v1
    Route::group([
        'namespace' => 'V1',
        'prefix'    => 'v1',
    ], function () {
        Route::resource('notification', 'NotificationController', ['except' => [
            'create', 'edit',
        ]]);
    });
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
 */

Route::group(['middleware' => 'web'], function () {
    Route::get('/', function () {
        return view('welcome');
    });

    Route::auth();

    Route::get('/home', 'HomeController@index');
});
