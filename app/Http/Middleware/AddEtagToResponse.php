<?php

namespace App\Http\Middleware;

use Closure;

class AddEtagToResponse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if ($request->isMethod('GET')) {
            $response->setEtag(md5($response->getContent()));
            $response->setPublic();
            $response->isNotModified($request);
        }

        return $response;
    }
}
