<?php

namespace App\Repositories;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Repository
{
    use AuthorizesRequests;
}
