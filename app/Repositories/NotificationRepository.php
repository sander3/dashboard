<?php

namespace App\Repositories;

use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class NotificationRepository extends Repository
{
    /**
     * Get the notifications associated with the authenticated user.
     *
     * @return Collection
     */
    public function getAuthenticatedUserNotifications()
    {
        return ['data' => Auth::user()->notifications];
    }

    /**
     * Associate a newly created notification with the authenticated user.
     *
     * @param  Request $request
     * @return array
     */
    public function associateAuthenticatedUserNotification(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'summary'     => 'required|string|min:5|max:140',
            'description' => 'required|string|min:10',
        ]);

        if ($validator->fails()) {
            return ['errors' => $validator->errors()];
        }

        return ['data' => Auth::user()->notifications()->create($request->all())];
    }

    /**
     * Get the specified notification associated with the authenticated user.
     *
     * @param  int $id
     * @return array
     */
    public function getAuthenticatedUserNotification($id)
    {
        $notification = Notification::findOrFail($id);

        $this->authorize('show', $notification);

        return ['data' => $notification];
    }

    /**
     * Update the specified notification associated with the authenticated user.
     *
     * @param  Request $request
     * @param  int  $id
     * @return array
     */
    public function updateAuthenticatedUserNotification(Request $request, $id)
    {
        $notification = Notification::findOrFail($id);

        $this->authorize('update', $notification);

        if ($notification->sent) {
            return ['errors' => [trans('errors.already_sent')]];
        }

        $validator = Validator::make($request->all(), [
            'summary'     => 'required_without:description|string|min:5|max:140',
            'description' => 'required_without:summary|string|min:10',
        ]);

        if ($validator->fails()) {
            return ['errors' => $validator->errors()];
        }

        return ['data' => $notification->update($request->all())];
    }

    /**
     * Remove the specified notification associated with the authenticated user.
     *
     * @param  int  $id
     * @return array
     */
    public function destroyAuthenticatedUserNotification($id)
    {
        return ['data' => Notification::findOrFail($id)->delete()];
    }
}
